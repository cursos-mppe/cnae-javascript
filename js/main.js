/**
 *
 *
 */
$(document).ready(function(){

    var plantel = new Plantel(
            'OD0132694',
            'UEN Simón Bolívar',
            500,
            'Juanito Alimaña',
            'Distrito Capital'
        );
    plantel.crearNuevo();


    vista.mostrarListaDePlanteles();

    $("#btnNuevoPlantel").on("click", function(evt){
        vista.mostrarFormulario();
    });

    $("#btnCancelarForm").on("click", function(evt){
        vista.ocultarFormulario();
        vista.resetearFormulario();
    })

    $("#plantelForm").on("submit", function(evento){
        evento.preventDefault();
        // Obtenemos los datos del formulario
        var codigo = $("#codigo").val();
        var nombre = $("#nombre").val();
        var matricula = $("#matricula").val();
        var director = $("#director").val();
        var estado = $("#estado").val();

        // Creamos un nuevo plantel
        var plantel = new Plantel(
            codigo,
            nombre,
            matricula,
            director,
            estado
        );

        // Guardamos el nuevo plantel
        plantel.crearNuevo();

        // Mostramos todos los planteles
        vista.mostrarListaDePlanteles();

        vista.ocultarFormulario();

        // Limpiamos el formulario
        vista.resetearFormulario();

    });

    $(".editar").on("click", function(){
        console.log($(this).attr("data-json"));
        var data = eval("("+$(this).attr("data-json")+")");
        $("#id").val(data.id);
        $("#codigo").val(data.codigo);
        $("#nombre").val(data.nombre);
        $("#matricula").val(data.matricula);
        $("#director").val(data.director);
        $("#estado").val(data.estado);
        vista.mostrarFormulario();
    });

});

vista = {
    mostrarFormulario: function(){
        if($("#plantelForm").hasClass('hide')){
            $("#plantelForm").removeClass('hide');
            $("#btnNuevoPlantel").addClass('hide');
        }
    },
    ocultarFormulario: function(){
        if(!$("#plantelForm").hasClass('hide')){
            $("#plantelForm").addClass('hide');
            $("#btnNuevoPlantel").removeClass('hide');
        }
    },
    resetearFormulario: function(){
        $("#id").val("");
        $("#codigo").val("");
        $("#nombre").val("");
        $("#matricula").val("");
        $("#director").val("");
        $("#estado").val("");
    },
    mostrarListaDePlanteles: function () {
        var plantelObj = new Plantel();
        var listaDePlanteles = plantelObj.obtenerTodos();
        var htmlDatos = "";
        if(listaDePlanteles.length==0){
            htmlDatos = '<tr>\
                <td colspan="5">\
                    <center>\
                        No Existen Planteles Registrados\
                    </center>\
                </td>\
            </tr>';
        }
        else{
            var plantel = null;
            for(var i=0; i<listaDePlanteles.length; i++) {
                plantel = listaDePlanteles[i];
                htmlDatos = htmlDatos + '<tr id="plantel-'+plantel.id+'">\
                    <td>\
                        <div class="col-xs-12">\
                            ' + plantel.codigo +'\
                        </div class="col-xs-12">\
                    </td>\
                    <td>\
                        <div class="col-xs-12">\
                            ' + plantel.nombre +'\
                        </div class="col-xs-12">\
                    </td>\
                    <td>\
                        <div class="col-xs-12">\
                            ' + plantel.director +'\
                        </div>\
                    </td>\
                    <td>\
                        <div class="col-xs-12">\
                            ' + plantel.estado +'\
                        </div>\
                    </td>\
                    <td>\
                        <div class="col-xs-5">\
                            <a href="#" class="editar" data-json=\'' + JSON.stringify(plantel) +'\' data-tr="plantel-' + plantel.id + '" data-index="'+i+'"><i class="glyphicon glyphicon-pencil"></i> Editar</a>\
                        </div>\
                        <div class="col-xs-1">\
                         \
                        </div>\
                        <div class="col-xs-5 col-xs-offset-1"> \
                            <a href="#" class="eliminar text-danger" data-json=\'' + JSON.stringify(plantel) +'\' data-tr="plantel-' + plantel.id + '" data-index="'+i+'"><i class="danger glyphicon glyphicon-trash"></i> Eliminar</a>\
                        </div>\
                    </td>\
                </tr>';
            };
        }
        $("#listaDePlanteles").html(htmlDatos);
    }
};