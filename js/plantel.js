/**
 *
 *
 */
var planteles = {
    index : 0,
    datos : new Array()
};

/**
 *
 */
function Plantel(codigo, nombre, matricula, director, estado){

    // Attributes
    planteles.index = planteles.index + 1;
    this.id = planteles.index;
    this.codigo = codigo;
    this.nombre = nombre;
    this.matricula = matricula;
    this.director = director;
    this.estado = estado;

    // Methods
    this.obtenerTodos = function(){
        return planteles.datos;
    };

    this.crearNuevo = function(){
        planteles.datos.push(this);
    }

    this.actualizar = function(){

    }

    this.eliminar = function(){

    }

}